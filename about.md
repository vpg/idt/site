---
layout: page
title: Az IDT-ről
permalink: /about
---

## Az IDT célja
* Olyan diákmozgalom létrehozása, melyben a tagok, és rajtuk keresztül az iskola diákjai örömmel és aktívan vállalnak feladatokat.
* Tevékenyen részt vesznek kreatív ötleteik megvalósításában, fejlesztve ezáltal személyiségüket.
* Olyan iskolát szeretnénk, ahol az együttműködés, és az összetartozás a domináns, ahová tanár, diák is jókedvvel lép be, örömmel és szívesen dolgozik együtt, ahol a szeretetteljes bánásmód, a türelem, az egymással szembeni megértés, a tolerancia a jellemző.

## Az IDT feladatai
* A Diákönkormányzat rendezvényeinek megszervezése, irányítása (*DÖNCI*)
* A diákok érdekérvényesítési és jogorvoslati ügyeinek továbbvitele
* Az osztályközösségek, az „iskolaközösség” kialakítása, formálása

## Az IDT jogi szempontból
Az iskolánkban a diákok érdekképviseleti és érdekérvényesítési jogát a Diákönkormányzat intézménye biztosítja. Ennek működése elsősorban az IDT (Iskolai Diák Tanács) formájában valósul meg, így a két fogalom ugyanazt a csoportot takarja.

Az IDT és a Diákönkormányzat létét, működését és kötelezettségiet elsősorban [a házirend](https://verespg.hu/sites/default/files/kozos/vpg-hazirend-20190205.pdf){:target='_top'} 1.3 és 1.4 pontja tartalmazza, de az [SzMSz](https://verespg.hu/sites/default/files/kozos/szmsz-20170901.pdf){:target='_top'} is tartalmaz néhány utalást.

### Az IDT jogai
* Az IDT működésének a feltételeit az iskola biztosítja. Ez jelenti a szükséges termet és az anyagi támogatást.
* Az IDT számára biztosított egy, az IDT munkáját segítő tanár (2019/20: Fekete Bálint Tanár Úr)
* Az IDT bekapcsolódhat a könyvtár működésének megszervezésébe:
  * Javaslatot tehet a könyvtári állományra,
  * és a könyvtár nyitvatartására
* SzMSz- és házirendmódosítást az IDT kezdeményezhet és SzMSz- illetve házirendmódosítás csak az IDT jóváhagyásával történhet

### Az IDT kötelezettségei
* Az IDT-nek van vezetője (2019/20: Tóth Júlia (11.a), elnök)
* Az IDT évente kétszer beszámolót tart működéséről és egyszer konzultál az igazgatóval
* Az IDT saját SzMSz-szel rendelkezik
* Az IDT-nek van éves munkaterve
* Az IDT évente egyszer összehívja a diákközgyűlést és a gyűlésen jegyzőkönyvet vezet
