---
layout: default
---

# IDT

## Aktualitások
{% assign actual_posts = site.posts | where: 'actual', true %}
{% if actual_posts.size > 0 %}
  {% for post in actual_posts %}
* {{ post.short }} [Bővebben &gt;&gt;&gt;]({{ post.url | absolute_url }})
  {% endfor %}
{% else %}
<small>_Jelenleg itt nincs mit olvasni. Nézz vissza később, mondjuk gyűlés után!_</small>
{% endif %}

{% assign non_actual_posts = site.posts | where: 'actual', false %}
{% if non_actual_posts.size > 0 %}
<details>
  <summary>Egyéb posztok</summary>
  {% for post in non_actual_posts %}
  * _{{ post.date }}_ {{ post.short }} [Részletek &gt;&gt;&gt;]({{ post.url | absolute_url }})
  {% endfor %}
</details>
{% endif %}

## Az IDT-ről
Az Iskolai Diák Tanács (röviden IDT) a Békásmegyeri Veres Péter Gimnáziumban a diákok érdekképviseleti tömörületének (a Diákönkormányzatnak) a működésére utal. Fő feladatunk a DÖNCI (Diákönkormányzati Nap) megszervezése, de nyitottak vagyunk bármilyen megoldandó problémára. Jogainkat és működésünket a Házirend és az SzMSz biztosítja. [További információ itt &gt;&gt;&gt;]({{ site.url }}{{ site.baseurl }}{% link about.md %})

## Az IDT működése

### Jegyzőkönyvek
Az IDT üléseiről és gyűléseiről időnként jegyzőkönyv készül.
Ezek elolvasását javasoljuk azoknak az IDT tagoknak, akik az adott gyűlésen nem tudtak részt venni illetve azon osztályok tanulóinak, ahol nem működik kellőképpen az ODB intézménye.

[A jegyzőkönyvek a {{ site.url }}{{ site.baseurl }}{% link verbatims.md %} oldalon kerülnek publikálásra. &gt;&gt;&gt;]({{ site.url }}{{ site.baseurl }}{% link verbatims.md %})

### Tagok
Az IDT-t osztályonként két választott képviselő és egy segítő tanár alkotja.

Az IDT elnöke: **Tóth Júlia** (11.a)  
Az IDT segítő tanára: **Fekete Bálint** Tanár Úr

### Ülések
_Vátozás:_  
Az IDT normális körülmények közt minden **_szerdán_ a nyolcadik órában** ülésezik.
