---
layout: page
title: Jegyzőkönyvek
permalink: /verbatims
---

# Jegyzőkönyvek

Az alábbi táblázatban megtalálhatóak azok a dokumentumok, amelyekből jegyzőkönyvként funkcionálhatnak.

Az itt összegyűjtött információk messze nem teljes körűek és helyességük nem garantált.

Dátum      | Link                    | Összefoglaló
:---------:|:-----------------------:|:------------
2019-02-06 | <http://bit.ly/2TBX9B6> | [DÖNCI megbeszélése, kitalálása](https://drive.google.com/file/d/1IIMyqc2rtX_LhzN_m-0xkKKQgdmToxX0/view){:target='_top'}
2019-02-26 | <http://bit.ly/2tQgdgH> | [Levél azoknak az IDT tagoknak, akik nem vettek részt a DÖNCI szervezésében és mégis van véleményük (teendők, érvek)](https://docs.google.com/document/d/1bxO2K4KvZbDoQiYsKLaT2fBTAygIU6q5HgKr0M74HH4/edit?usp=sharing){:target='_top'}
2019-02-17 | <http://bit.ly/2EQctSr> | [ÓBIÖK - továbbítandó információk](https://drive.google.com/open?id=1nRQ431W_O6DJdooYHoaGPvlKWip8tA9JYL0h1dzxnF0){:target='_top'}
2019-02-27 | <http://bit.ly/2C5eXL6> | [IDT ülések időpontja, új DÖNCI kérdéseinek eloszlatása, ÓBIÖK infók ismertetése](https://diakoffice-my.sharepoint.com/:w:/g/personal/edu_pqqy_6669_diakoffice_onmicrosoft_com/EQD7KuQb6xFArPBnXRQmzwsBGxCa5Ra-G7rIWDIp6cXGwg?e=Etk0aT){:target='_top'}
2019-03-06 | <http://bit.ly/2tPXjq8> | [Rövid gyűlés a pedagógiai program változásáról és az IDT-s "intenzív nap" időpontjának meghatározása](https://diakoffice-my.sharepoint.com/:w:/g/personal/edu_pqqy_6669_diakoffice_onmicrosoft_com/ESQKbCA7CuBPgciNwRkgmV0B7KXsus7Jad7AR259BOy4jg?e=KIbpwD){:target='_top'}
2019-03-20 | <http://bit.ly/2Tjzami> | [Intenzív nap: DÖNCI szervezés, társasjáték est, Veres Beach és miegymás](https://diakoffice-my.sharepoint.com/:w:/g/personal/edu_pqqy_6669_diakoffice_onmicrosoft_com/EVPwOhsVfx5Hih2fWXatwNIBAjGPhwEyXcYWFZqxTRVHnA){:target='_top'}
2019-03-27 | <http://bit.ly/2V3uUZS> | [{{ site.posts | where: "title", "Jegyzőkönyv 2019. 3. 27., Szerda" | map: "short" | first }}]({{ site.posts | where: "title", "Jegyzőkönyv 2019. 3. 27., Szerda" | map: "url" | first | absolute_url }}){:target='_top'}
2019-05-22 | <http://bit.ly/2wfpW16> | [Társasjáték est elõtti megbeszélés](https://diakoffice-my.sharepoint.com/:w:/g/personal/edu_pqqy_6669_diakoffice_onmicrosoft_com/EbnDkiO23oFIuPJVF0e4LosBh27LHZ6RvK0Ina9vLlfGHg?e=FHQpuX){:target='_top'}
2019-06-05 | <http://termbin.com/hig9> | Veres Beach és táblafeliratok versenye
2019-09-16 | <http://bit.ly/2m2doYZ> | [ÓBIÖK, gólyahét, öregdiák-találkozó, pulcsi, farsang](https://gist.github.com/lezsakdomi/cff287dc8328912b855f9e6c81695ca3)
2019-09-23 | | https://gist.github.com/lezsakdomi/8c257df108b403ab0f26ba1916709857
2020-01-31 | | [{{ site.posts | where: "title", "Jegyzőkönyv 2020. 1. 31., Péntek" | map: "short" | first }}]({{ site.posts | where: "title", "Jegyzőkönyv 2020. 1. 31., Péntek" | map: "url" | first | absolute_url }}){:target='_top'}
2020-02-12 | | [{{ site.posts | where: "title", "Jegyzőkönyv 2020. 2. 12., Szerda" | map: "short" | first }}]({{ site.posts | where: "title", "Jegyzőkönyv 2020. 2. 12., Szerda" | map: "url" | first | absolute_url }}){:target='_top'}
2020-02-26 | | [{{ site.posts | where: "title", "Jegyzőkönyv 2020. 2. 26., Szerda" | map: "short" | first }}]({{ site.posts | where: "title", "Jegyzőkönyv 2020. 2. 26., Szerda" | map: "url" | first | absolute_url }}){:target='_top'}
2020-04-04 | | [IDT gyűlés](https://docs.google.com/document/d/1e-LEAgPrTE0N6VWnZ8AwGluv8-UXjylK-rk6IjjSn1Q/edit?usp=sharing)
2020-03-11 | | [DÖNCI csúsztatós/lemondós IDT gyűlés](https://docs.google.com/document/d/1blj1Kmic1uEnm8R9y0_xjZkDjicHP8kdgT8dVXQNWOs/edit)
