window.addEventListener("message", event => {
    if (typeof event.data == "string") {
        switch (event.data) {
            case 'request-scrollHeight':
                event.source.postMessage({
                    documentDetails: {
                        scrollHeight: document.body.scrollHeight
                    }
                }, "*");
        }
    }
});
