---
layout: post
title:  "Jegyzőkönyv 2020. 2. 12., Szerda"
date:   2019-02-12 15:14:00 +0100
categories: verbatim
short:  "IDT"
---

1. Pulcsik
	- Meg mindig nem vittek el paran
	- Ha ismerunk par embert, probaljuk szemelyesen
	- Nem mindenki diak, hanem van szuloi nev is
	- Addig nem lesz extra utorendeles, amig el nem fogynak ezek a pulcsik
	- 3 honapig tartjak a szitat
	- Kaptunk 90000 Ft-ot

2. DONCI: Aprilis 8.
	- Meg nem vagyunk elcsuszva
	- Jovo het pentek bolyai
	- Tavaly a 11-esek nem voltak nagyon nyitottak, de igazabol mindegy...
	- Az jobb lenne, ha onekik csak felugyelniuk kellene
	- Legyenek otletek, de szervezzek is meg
	- Konkret otletek:
		- XBox
	- Tavalyi jo volt-e? -->  ...
	- A szavazasokat nem veszik komolyan
	- LESZ A TAVALYI
	- Lehet egy kicsit anyagit felhasznalni

3. Benak vagyunk
	- Mindent keson kezdunk el szervezni
	- Kezdjuk el most a tarsas estet?
		- Azzal az a baj, hogy "miert menjenek el"?
	- Nincs olyan program, amire jojjenek el a diakok
		- Otlet?
			- Bentalvos focizas, tarsasozas, stb.
			- Tobb napos
			- Meghirdetjuk elore, hogy rendelunk pizzat (...)
				- -> Mindenki hoz egy talca sutit
			- Sport ES tarsas
			- Kell felugyelo tanar
			- Ropi kozosseg epitobb
			- "Kozos ropibajnoksag es ordoguzes"
			- Marc. 13, P
			- Ropi + tarsas
			- Korosztaly?
				- 7-8
				- 9-12
				(kulon palyan)
				- Jelentkezes csapatban
					- Kulon is
				- 7.-tol folfele hirdetunk --> mindenki benn maradhat
