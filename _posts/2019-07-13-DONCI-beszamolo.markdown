---
layout: post
title:  "DÖNCI fotók"
date:   2019-07-13 16:12:00 +0100
categories: dönci fotók
actual: false
short: >
  A DÖNCI-n készült fotók ebben a posztban folyamatosan publikálásra kerülnek.
---

# DÖNCI

A 2019-es DÖNCI-t a hagyományoktól elszakadva rendeztük meg. Idén a diákokat csapatokra osztottuk és kötelezõ jelleggel osztottuk be õket páronként kompetitív feladatokra.

A beosztást illetve a csapatokat a [Nagy DÖNCI-s táblázat](https://diakoffice-my.sharepoint.com/:x:/g/personal/edu_pqqy_6669_diakoffice_onmicrosoft_com/ETtO2380L0dDn2-k2Pjyp9kB9awfWXCBsOy2s_T8mcf-Gg?e=D1KAi2) tartalmazza.

# DÖNCI fotók

A _Kreatív alkotás_ nevû állomáson az alábbi rajzok születtek: 
https://photos.app.goo.gl/xqQaQyd2CWDzCVVM7
