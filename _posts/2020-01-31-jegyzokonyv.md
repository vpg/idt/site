---
layout: post
title:  "Jegyzőkönyv 2020. 1. 31., Péntek"
date:   2019-01-31 10:55:00 +0100
categories: verbatim
short:  "Megbeszélés: DÖNCI"
---

# DONCI
Aprilis 8.

 A tanarok reszerol nagy siker volt a tavalyi.

DE egy keres volt: A termeket ne nekik kelljen megszervezni!

A diaksag ha azt mondja hogy ne legyen, akkor ne.

Ha diakonkormanyzati nap, a diakok mutassak meg, hogy meg tudjak csinalni. Nem minden tanar gondolja igy persze.
Oket beosztanank termekbe.

---

Tavaly 15 termet szerveztunk meg, a 12-ezeket hagyjuk

A 11-esek 5 termet szerveznek meg, 2 csapatnyi gyermek munkajat ossze lehet merni es a vegen egy zacsko cukorral jutalmazni.

Pl.: Kahoot, Just Dance, ...

30 an vannak, azaz kb. 6 embernek kell egy termet kitalalni.

Ha van par aktiv 12-es beszallhat, de nekik faktot szoktak szervezni pl . Biczo.

Jovo het hetfon vagy kedden bemondatjuk, es azon kell gondolkodniuk az osztalyokak hogy ezt meg tudjak-e szervezni.

Ha igen, ez lesz, ha nem akkor marad a regi fajta program (film, szinpadon bohockodas, stb.)

---

Valoszinuleg meg lehet csinalni, mert olyan mint a golyabal.

Volt mar olyan, hogy forditott nap: A tanarok es diakok szerepet cserelnek. (jokat lehet csinalni ha van par vallalkozo tanar)

Legyunk pozitivak! F. ad ennek a programnak meg egy eselyt,

Ha nem ugy dobjuk fel hogy meg tudjuk-e csinalni, hanem hogy ez a feladat, akkor szerencsesebb

stb.
