---
layout: post
title:  "Jegyzőkönyv 2019. 3. 27., Szerda"
date:   2019-03-27 19:00:00 +0100
categories: verbatim
short:  "Megbeszélés a szükséges nyomtatványokról"
---

# Ülés március 27-én

## Részletek

| **Megjelent:**    | Fekete Bálint az IDT munkáját segítő tanár és Lezsák Domonkos nem hivatalos ODB tag. |
| **Helyszín:**     | Könyvtári olvasó |
| **Kezete:**       | 8. óra kezdete után kicsivel |
| **Vége:**         | 15:15 illetve 15:30 |

## Napirendi pontok
- Társasjáték-est hirdetése
  - QR kód
  - Facebook bejegyzés
  - Plakát és egyéb vizuális megoldások
- DÖNCI szervezés
  - Csapatok és állomások párosítása, a végeredmény átnézése
  - Diákok eligazításának megszervezése
  - Tanárok leellenőrzése
  - Termek leellenőrzése
  - A 11. évfolyam helyzete
    - Kezdés eldöntése
    - Felosztás (ha kétfelé)
    - Film kiválasztása

### Döntések
- **Társasjáték-est hirdetése**  
  Tekintettel a kisszámú résztvevőre, a fontosabb feladatokra koncentráljunk.
- **DÖNCI szervezés**
  - **Csapatok és állomások párosítása, a végeredmény átnézése**  
    Nincsenek jelen az érintettek (ODB-sek) &rarr; nem lehet érdemben foglalkozni az üggyel
  - **Diákok eligazítása**
    - Legyen útilevél
      - Esetleg aláírásokkal
    - Előzetes tájékoztatás szükséges
      - Formája:
        - Plakát a bejáratnál _ÉS_ a tömeg elkerülésének az érdekében az ODBsek tájékoztatják az osztályokat.
  - **Tanárok leellenőrzése**  
    Szükséges egy papír alapú táblázat, ahol ezt lehet követni
  - **11. évfolyam**
    - **Kezdés eldöntése**  
      8:00-9:50 és 9:00-9:50  
      _Indíték: Az évfolyam legalább harmadát minimális erőszakkal már rá lehetne venni a 8 órai kezdésre_
    - **Felosztás**  
      - Érdeklődés szerint: Sportol (alternatíva: segít a lebonyolításban) vagy filmet szeretne nézni
      - Regisztráció alapján (papír alapú)
        - Megbeszéltük a formát is
      - Kvóta:
        - 45 fő sportra
        - 15-15 fő a két filmre
    - **Film kiválasztása**
      - Ösztön (1999)
        - Angolul, angol felirattal?
      - Moszkva tér (2001)  
        Csak 88 perc :( &rarr; max. az elején elhúzzuk az időt az indítással :D

### Akciók
- **Társasjáték-est hirdetése**
  - **QR kód**  
    Tervezés, nyomtatás megkezdése
- **DÖNCI szervezés**
  - **11. évfolyam**
    - **Felosztás**
      - Regisztrációs lap formatervezése

### Megjegyzések
- **Társasjáték-est hirdetése**
  - **QR kód**  
    _RENGETEG_ példány született
- **DÖNCI szervezés**
  - **Csapatok és állomások párosítása, a végeredmény átnézése**  
    Nincsenek jelen az érintettek (ODB-sek) &rarr; nem lehet érdemben foglalkozni az üggyel &rarr; majd Messengeren kitaláljuk
  - **Tanárok leellenőrzése**
  - **11. évfolyam**
    - **Kezdés**
      - A 8:00-as csapat előbb hazamehet
    - **Felosztás**
      - **Sportos csapat**  
        Ha nagyon reklamálnak, az előadás után lehet menni segíteni a tanároknak.
      - **Regisztrációs lap**  
        A füzetemben van egy vázlatos rajz. * - Lezsák Domonkos*
    - **Film kiválasztása**
      - Akár három film is lehet, ha ezt a termek engedik.
      - **Ösztön**  
        126 perc &rarr; jó!
      - **Lélektől lélekig**  
        117 perc &rarr; jó lenne, de nem javasolt az Ösztön mellett
      - **Moszkva tér**  
        Jó alternatíva a többi filmre

        Rövid :(

## Megmaradt tennivalók
- Társasjáték-est hirdetése
  - QR kód
    - Ollóval felvágás
    - Szórás
    - Esetleg dolgozatpapírok nyomtatása?
  - Facebook bejegyzés
  - Plakát és egyéb vizuális megoldások
- **DÖNCI szervezés**
  - Tanárregisztrációs táblázat
    - Tanárok leellenőrzése (utána)
  - Csapatok és állomások párosítása, a végeredmény átnézése
  - Útilevelek elkészítése
  - Termek leellenőrzése
  - **11. évfolyam**
    - Kezdés eldöntése
    - Felosztás
      - Űrlap elkészítése
      - Űrlap kinyomtatása és hirdetése
    - Film véglegesítése

## Jegyzet
Van, ha muszáj előkereshetem * - Lezsák Domonkos*
