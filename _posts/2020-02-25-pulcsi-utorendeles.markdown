---
layout: post
title: "VPG pulcsi utórendelés - második batch"
date: 2020-02-25 19:00:00 +0100
categories: form
actual: false
short: >
  Ha szeretnél VPG pulcsit utórendelni, add meg
  [itt](https://docs.google.com/forms/d/e/1FAIpQLSe--WzrvguHIOjMzpEvH1z9wCXRquP5PQBg42ylzeW00v4u8w/viewform?usp=sf_link){:target='_blank'}
  az email-címedet
---

# VPG pulcsi utórendelés #2
Elindult egy BTO a VPG pulcsik második utórendelésére, ez legalább 25 jelentkező
után lesz sikeres.

Itt lehet jelentkezni:
[u.nu/s8pzr](https://docs.google.com/forms/d/e/1FAIpQLSe--WzrvguHIOjMzpEvH1z9wCXRquP5PQBg42ylzeW00v4u8w/viewform?usp=sf_link)

Ettől függetlenül a következő rendes rendelés "esetleg talán" jövő év elején várható.
