---
layout: post
title:  "DÖNCI programötlet form"
date:   2019-02-26 20:00:00 +0100
categories: form
actual: false
short: >
  Elkészült az online **DÖNCI ötletgyűjtő kérdőív**.
  A papíron beküldött programjavaslatokat feltöltöttük, a feldolgozás folyamatban.
  ([Kitölthető itt.](https://docs.google.com/forms/d/e/1FAIpQLSckOJzh3C5aIvfeBmQ7AuZv7zx3Tb5OqjdYBn4R5g0afa3nTQ/viewform?usp=sf_link){:target='_blank'})
---

# DÖNCI programötletek beküldése
Ha bárkinek javaslata, kérése ill. ötlete van a DÖNCI programjait illetően, azt ossza meg velünk a következő kérdőív kitöltésével:
[3.ly/gf2](https://docs.google.com/forms/d/e/1FAIpQLSckOJzh3C5aIvfeBmQ7AuZv7zx3Tb5OqjdYBn4R5g0afa3nTQ/viewform?usp=sf_link){:taget='_blank'}

A beküldött javaslatokat folyamatosan dolgozzuk fel.
