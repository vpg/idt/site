---
layout: post
title:  "Jelentkezés a társasjáték-estre"
date:   2019-05-15 21:43:00 +0100
categories: form társasjáték-est
actual: false
permalink: /tarsasest-2019
short: >
  A **társasjáték-est** jelentkezési határideje _**2019. május 17. péntek.**_, elkészült a szülői beleeggyezési nyilatkozat.
---

# Társasjáték-est
Idén is megrendezésre kerül a tavaly nagy sikert aratott társasjáték-est.

Még több résztvevőre számítva idén a tornateremben és a táncteremben rendezzük meg az eseményt. Minden társasjátéknak lesz egy játékmestere, így biztosra vehetitek, hogy kiigazodtok a játékok között.

Az 5. – 8. osztályosok 4-től 8-ig, a 9. – 12. osztályosok utána, este 8-tól reggel 8-ig vehetnek részt a maratonon. 

Az est időpontja május 24., péntek.

Ha szeretnél részt venni, netán hozni valami játékot, még az előtte való héten (05.17-ig) szülővel töltsd ki az alábbi jelentkezési lapot és add le valamelyik ODB-snek.

#### A jelentkezési lap itt tölthető le:

[bit.ly/2VCmIUC](https://diakoffice-my.sharepoint.com/:w:/g/personal/edu_pqqy_6669_diakoffice_onmicrosoft_com/EREPoFSyns9KuXiki0IJuJ4BZiGFUN08JnP6fxT_L1Rr8A?e=sLASAy)
